# Salesforce Metadata

Vendor: Saleforce
Homepage: https://www.salesforce.com/

Product: Saleforce
Product Page: https://www.salesforce.com/

## Introduction
We classify Salesforce Metadata into the ITSM (Service Management) domain as it supports key ITSM practices, such as configuration management, change management and deployment

## Why Integrate
The Salesforce Metadata adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Saleforce to enable efficient management and deployment of configurations. With this adapter you have the ability to perform operations such as:

- Describe, Create, Update, or Delete Metadata
- Deployment

## Additional Product Documentation
The [API documents for Salesforce Metadata](https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/meta_types_list.htm)