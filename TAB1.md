# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Salesforce_metadata System. The API that was used to build the adapter for Salesforce_metadata is usually available in the report directory of this adapter. The adapter utilizes the Salesforce_metadata API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Salesforce Metadata adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Saleforce to enable efficient management and deployment of configurations. With this adapter you have the ability to perform operations such as:

- Describe, Create, Update, or Delete Metadata
- Deployment

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
