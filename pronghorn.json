{
  "id": "@itentialopensource/adapter-salesforce_metadata",
  "type": "Adapter",
  "export": "SalesforceMetadata",
  "title": "Salesforce_metadata",
  "src": "adapter.js",
  "roles": [
    "admin"
  ],
  "methods": [
    {
      "name": "iapUpdateAdapterConfiguration",
      "summary": "Updates the adapter configuration",
      "description": "Updates the adapter configuration file with the provided changes",
      "input": [
        {
          "name": "configFile",
          "type": "string",
          "info": "The name of the file to change",
          "required": true,
          "schema": {
            "title": "configFile",
            "type": "string"
          }
        },
        {
          "name": "changes",
          "type": "object",
          "info": "JSON object containing the configuration changes",
          "required": true,
          "schema": {
            "title": "changes",
            "type": "object"
          }
        },
        {
          "name": "entity",
          "type": "string",
          "info": "The entity in which the changes are being made",
          "required": false,
          "schema": {
            "title": "entity",
            "type": "string"
          }
        },
        {
          "name": "type",
          "type": "string",
          "info": "The type of file to change - action, schema, or mock",
          "required": false,
          "schema": {
            "title": "type",
            "type": "string"
          }
        },
        {
          "name": "action",
          "type": "string",
          "info": "The action to be changed",
          "required": false,
          "schema": {
            "title": "action",
            "type": "string"
          }
        },
        {
          "name" : "replace",
          "type": "boolean",
          "info": "True to replace entire mock data, false to merge/append",
          "required": false,
          "schema": {
            "title": "replace",
            "type" : "boolean"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapUpdateAdapterConfiguration"
      },
      "task": true
    },
    {
      "name": "iapSuspendAdapter",
      "summary": "Suspends the adapter",
      "description": "Suspends the adapter",
      "input": [
        {
          "name": "mode",
          "type": "enum",
          "enumerals": [
            "pause",
            "error"
          ],
          "info": "How incoming requests are handled. Defaults to 'pause'",
          "description": "How incoming requests are handled. Defaults to 'pause'",
          "schema": {
            "title": "mode",
            "type": "string"
          },
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing the adapter suspended status",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapSuspendAdapter"
      },
      "task": true
    },
    {
      "name": "iapUnsuspendAdapter",
      "summary": "Unsuspends the adapter",
      "description": "Unsuspends the adapter",
      "input": [],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing the adapter suspended status",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapUnsuspendAdapter"
      },
      "task": true
    },
    {
      "name": "iapGetAdapterQueue",
      "summary": "Return the requests that are waiting in the queue if throttling is enabled",
      "description": "Return the requests that are waiting in the queue if throttling is enabled",
      "input": [],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing the adapter queue",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapGetAdapterQueue"
      },
      "task": true
    },
    {
      "name": "iapFindAdapterPath",
      "summary": "Provides the ability to see if a particular API path is supported by the adapter",
      "description": "Provides the ability to see if a particular API path is supported by the adapter",
      "input": [
        {
          "name": "apiPath",
          "type": "string",
          "info": "The API Path you want to check - make sure to not include base path and version",
          "description": "The API Path you want to check - make sure to not include base path and version",
          "schema": {
            "title": "apiPath",
            "type": "string"
          },
          "required": true
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapFindAdapterPath"
      },
      "task": true
    },
    {
      "name": "iapTroubleshootAdapter",
      "summary": "Runs troubleshoot script for adapter",
      "description": "Runs troubleshoot script for adapter",
      "input": [
        {
          "name": "props",
          "type": "object",
          "info": "Object containing configuration, healthcheck and auth properties {'connProps':{'host': 'api.service.com', 'base_path': '/', 'protocol': 'http', 'port': 443, 'version': 'v1'},'healthCheckEndpoint': '/healthcheck', 'auth': {'auth_method': 'no authentication', 'username': 'username', 'password': 'password'}}",
          "required": true,
          "schema": {
            "title": "props",
            "type": "object"
          }
        },
        {
          "name": "persistFlag",
          "type": "boolean",
          "info": "Whether the input properties should be saved",
          "required": true,
          "schema": {
            "title": "persistFlag",
            "type": "boolean"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing the test results",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapTroubleshootAdapter"
      },
      "task": true
    },
    {
      "name": "iapRunAdapterHealthcheck",
      "summary": "Runs healthcheck script for adapter",
      "description": "Runs healthcheck script for adapter",
      "input": [],
      "output": {
        "name": "result",
        "type": "boolean",
        "description": "Whether healthcheck passed or failed",
        "schema": {
          "title": "result",
          "type": "boolean"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapRunAdapterHealthcheck"
      },
      "task": true
    },
    {
      "name": "iapRunAdapterConnectivity",
      "summary": "Runs connectivity check script for adapter",
      "description": "Runs connectivity check script for adapter",
      "input": [],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing the test results",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapRunAdapterConnectivity"
      },
      "task": true
    },
    {
      "name": "iapRunAdapterBasicGet",
      "summary": "Runs basicGet script for adapter",
      "description": "Runs basicGet script for adapter",
      "input": [],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing the test results",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapRunAdapterBasicGet"
      },
      "task": true
    },
    {
      "name": "iapMoveAdapterEntitiesToDB",
      "summary": "Moves entities from an adapter into the IAP database",
      "description": "Moves entities from an adapter into the IAP database",
      "input": [],
      "output": {
        "name": "res",
        "type": "object",
        "description": "A JSON Object containing status, code and the response from the mongo transaction",
        "schema": {
          "title": "res",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapMoveAdapterEntitiesToDB"
      },
      "task": true
    },
    {
      "name": "iapDeactivateTasks",
      "summary": "Deactivate the inputted tasks",
      "description": "Deactivate the inputted tasks",
      "input": [
        {
          "name": "tasks",
          "type": "array",
          "description": "Tasks to deactivate",
          "schema": {
            "title": "tasks",
            "type": "array"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing success status",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapDeactivateTasks"
      },
      "task": true
    },
    {
      "name": "iapActivateTasks",
      "summary": "Activate the inputted tasks",
      "description": "activate the inputted tasks",
      "input": [
        {
          "name": "tasks",
          "type": "array",
          "description": "Tasks to activate",
          "schema": {
            "title": "tasks",
            "type": "array"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing success status",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapActivateTasks"
      },
      "task": true
    },
    {
      "name": "iapPopulateEntityCache",
      "summary": "Populate the cache for the given entities",
      "description": "Populate the cache for the given entities",
      "input": [
        {
          "name": "entityTypes",
          "type": "array",
          "info": "the entity type(s) to populate",
          "required": true,
          "schema": {
            "title": "entityTypes",
            "type": "array"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapPopulateEntityCache"
      },
      "task": true
    },
    {
      "name": "iapRetrieveEntitiesCache",
      "summary": "Retrieves data from cache for specified entity type",
      "description": "Retrieves data from cache for specified entity type",
      "input": [
        {
          "name": "entityType",
          "type": "string",
          "info": "entity of which to retrieve",
          "required": true,
          "schema": {
            "title": "entityType",
            "type": "string"
          }
        },
        {
          "name": "options",
          "type": "object",
          "info": "settings of which data to return and how to return it",
          "required": false,
          "schema": {
            "title": "options",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapRetrieveEntitiesCache"
      },
      "task": true
    },
    {
      "name": "getDevice",
      "summary": "Get the Appliance",
      "description": "Get the Appliance",
      "input": [
        {
          "name": "deviceName",
          "type": "string",
          "info": "An Appliance Device Name",
          "required": true,
          "schema": {
            "title": "deviceName",
            "type": "string"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/getDevice"
      },
      "task": false
    },
    {
      "name": "getDevicesFiltered",
      "summary": "Get Appliances that match the filter",
      "description": "Get Appliances that match the filter",
      "input": [
        {
          "name": "options",
          "type": "object",
          "info": "options - e.g. { 'start': 1, 'limit': 20, 'filter': { 'name': 'abc123' } }",
          "required": true,
          "schema": {
            "title": "options",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "array",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "array"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/getDevicesFiltered"
      },
      "task": false
    },
    {
      "name": "isAlive",
      "summary": "Checks the status for the provided Appliance",
      "description": "Checks the status for the provided Appliance",
      "input": [
        {
          "name": "deviceName",
          "type": "string",
          "info": "An Appliance Device Name",
          "required": true,
          "schema": {
            "title": "deviceName",
            "type": "string"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "boolean",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "boolean"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/isAlive"
      },
      "task": false
    },
    {
      "name": "getConfig",
      "summary": "Gets a config for the provided Appliance",
      "description": "Gets a config for the provided Appliance",
      "input": [
        {
          "name": "deviceName",
          "type": "string",
          "info": "An Appliance Device Name",
          "required": true,
          "schema": {
            "title": "deviceName",
            "type": "string"
          }
        },
        {
          "name": "format",
          "type": "string",
          "info": "The format to be returned - this is ignored as we always return json",
          "required": false,
          "schema": {
            "title": "format",
            "type": "string"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/getConfig"
      },
      "task": false
    },
    {
      "name": "iapGetDeviceCount",
      "summary": "Gets a device count from the system",
      "description": "Gets a device count from the system",
      "input": [],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapGetDeviceCount"
      },
      "task": false
    },
    {
      "name": "iapExpandedGenericAdapterRequest",
      "summary": "Makes the requested generic call with additional options",
      "description": "Makes the requested generic call with additional options via metadata",
      "input": [
        {
          "name": "metadata",
          "type": "object",
          "info": "metadata for the call (optional)",
          "description": "metadata for the call - allows for many enhancements (optional)",
          "schema": {
            "title": "metadata",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "uriPath",
          "type": "string",
          "info": "the path of the api call - do not include the host, port, base path or version",
          "description": "the path of the api call",
          "schema": {
            "title": "uriPath",
            "type": "string"
          },
          "required": false
        },
        {
          "name": "restMethod",
          "type": "string",
          "info": "the rest method (GET, POST, PUT, PATCH, DELETE)",
          "description": "the rest method (GET, POST, PUT, PATCH, DELETE)",
          "schema": {
            "title": "restMethod",
            "type": "string"
          },
          "required": false
        },
        {
          "name": "pathVars",
          "type": "object",
          "info": "the parameters to be put within the url path (optional)",
          "description": "the parameters to be put within the url path (optional)",
          "schema": {
            "title": "pathVars",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "queryData",
          "type": "object",
          "info": "the query parameters to be put on the url (optional)",
          "description": "the query parameters to be put on the url (optional)",
          "schema": {
            "title": "queryData",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "requestBody",
          "type": "object",
          "info": "the payload to be sent with the request (optional)",
          "description": "the payload to be sent with the request (optional)",
          "schema": {
            "title": "requestBody",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "addlHeaders",
          "type": "object",
          "info": "additional headers to be put on the call (optional)",
          "description": "additional headers to be put on the call (optional)",
          "schema": {
            "title": "addlHeaders",
            "type": "object"
          },
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/iapExpandedGenericAdapterRequest"
      },
      "task": true
    },
    {
      "name": "genericAdapterRequest",
      "summary": "Makes the requested generic call",
      "description": "Makes the requested generic call",
      "input": [
        {
          "name": "uriPath",
          "type": "string",
          "info": "the path of the api call - do not include the host, port, base path or version",
          "description": "the path of the api call",
          "schema": {
            "title": "uriPath",
            "type": "string"
          },
          "required": true
        },
        {
          "name": "restMethod",
          "type": "string",
          "info": "the rest method (GET, POST, PUT, PATCH, DELETE)",
          "description": "the rest method (GET, POST, PUT, PATCH, DELETE)",
          "schema": {
            "title": "restMethod",
            "type": "string"
          },
          "required": true
        },
        {
          "name": "queryData",
          "type": "object",
          "info": "the query parameters to be put on the url (optional)",
          "description": "the query parameters to be put on the url (optional)",
          "schema": {
            "title": "queryData",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "requestBody",
          "type": "object",
          "info": "the payload to be sent with the request (optional)",
          "description": "the payload to be sent with the request (optional)",
          "schema": {
            "title": "requestBody",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "addlHeaders",
          "type": "object",
          "info": "additional headers to be put on the call (optional)",
          "description": "additional headers to be put on the call (optional)",
          "schema": {
            "title": "addlHeaders",
            "type": "object"
          },
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/genericAdapterRequest"
      },
      "task": true
    },
    {
      "name": "genericAdapterRequestNoBasePath",
      "summary": "Makes the requested generic call",
      "description": "Makes the requested generic call",
      "input": [
        {
          "name": "uriPath",
          "type": "string",
          "info": "the path of the api call - do not include the host, port, base path or version",
          "description": "the path of the api call",
          "schema": {
            "title": "uriPath",
            "type": "string"
          },
          "required": true
        },
        {
          "name": "restMethod",
          "type": "string",
          "info": "the rest method (GET, POST, PUT, PATCH, DELETE)",
          "description": "the rest method (GET, POST, PUT, PATCH, DELETE)",
          "schema": {
            "title": "restMethod",
            "type": "string"
          },
          "required": true
        },
        {
          "name": "queryData",
          "type": "object",
          "info": "the query parameters to be put on the url (optional)",
          "description": "the query parameters to be put on the url (optional)",
          "schema": {
            "title": "queryData",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "requestBody",
          "type": "object",
          "info": "the payload to be sent with the request (optional)",
          "description": "the payload to be sent with the request (optional)",
          "schema": {
            "title": "requestBody",
            "type": "object"
          },
          "required": false
        },
        {
          "name": "addlHeaders",
          "type": "object",
          "info": "additional headers to be put on the call (optional)",
          "description": "additional headers to be put on the call (optional)",
          "schema": {
            "title": "addlHeaders",
            "type": "object"
          },
          "required": false
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/genericAdapterRequestNoBasePath"
      },
      "task": true
    },
    {
      "name": "iapRunAdapterLint",
      "summary": "Run the adapter lint script to return the results",
      "description": "Run the adapter lint script to return the results",
      "input": [],
      "output": {
        "name": "result",
        "type": "string",
        "description": "A string containing the run results",
        "schema": {
          "title": "result",
          "type": "string"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "GET",
        "path": "/iapRunAdapterLint"
      },
      "task": true
    },
    {
      "name": "iapRunAdapterTests",
      "summary": "Run the adapter test scripts (baseunit and unit) to return the results",
      "description": "Run the adapter test scripts (baseunit and unit) to return the results",
      "input": [],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "GET",
        "path": "/iapRunAdapterTests"
      },
      "task": true
    },
    {
      "name": "iapGetAdapterInventory",
      "summary": "Provide inventory information abbout the adapter",
      "description": "Provide inventory information abbout the adapter",
      "input": [],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "GET",
        "path": "/iapGetAdapterInventory"
      },
      "task": true
    },
    {
      "name": "postCancelDeploy",
      "summary": "cancelDeploy",
      "description": "Cancels a metadata deploy.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postCancelDeploy"
      },
      "task": true
    },
    {
      "name": "postCheckDeployStatus",
      "summary": "checkDeployStatus",
      "description": "Check the current status of an asyncronous deploy call.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postCheckDeployStatus"
      },
      "task": true
    },
    {
      "name": "postCheckRetrieveStatus",
      "summary": "checkRetrieveStatus",
      "description": "Check the current status of an asyncronous deploy call.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postCheckRetrieveStatus"
      },
      "task": true
    },
    {
      "name": "postCreateMetadata",
      "summary": "createMetadata",
      "description": "Creates metadata entries synchronously.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postCreateMetadata"
      },
      "task": true
    },
    {
      "name": "postDeleteMetadata",
      "summary": "deleteMetadata",
      "description": "Deletes metadata entries synchronously.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postDeleteMetadata"
      },
      "task": true
    },
    {
      "name": "postDeploy",
      "summary": "deploy",
      "description": "Deploys a zipfile full of metadata entries asynchronously.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postDeploy"
      },
      "task": true
    },
    {
      "name": "postDeployRecentValidation",
      "summary": "deployRecentValidation",
      "description": "Deploys a previously validated payload without running tests.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postDeployRecentValidation"
      },
      "task": true
    },
    {
      "name": "postDescribeMetadata",
      "summary": "describeMetadata",
      "description": "Describes features of the metadata API.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postDescribeMetadata"
      },
      "task": true
    },
    {
      "name": "postDescribeValueType",
      "summary": "describeValueType",
      "description": "Describe a complex value type",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postDescribeValueType"
      },
      "task": true
    },
    {
      "name": "postListMetadata",
      "summary": "listMetadata",
      "description": "Lists the available metadata components.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postListMetadata"
      },
      "task": true
    },
    {
      "name": "postReadMetadata",
      "summary": "readMetadata",
      "description": "Reads metadata entries synchronously.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postReadMetadata"
      },
      "task": true
    },
    {
      "name": "postRenameMetadata",
      "summary": "renameMetadata",
      "description": "Renames a metadata entry synchronously.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postRenameMetadata"
      },
      "task": true
    },
    {
      "name": "postRetrieve",
      "summary": "retrieve",
      "description": "Retrieves a set of individually specified metadata entries.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postRetrieve"
      },
      "task": true
    },
    {
      "name": "postUpdateMetadata",
      "summary": "updateMetadata",
      "description": "Updates metadata entries synchronously.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postUpdateMetadata"
      },
      "task": true
    },
    {
      "name": "postUpsertMetadata",
      "summary": "upsertMetadata",
      "description": "Upserts metadata entries synchronously.",
      "input": [
        {
          "name": "body",
          "type": "object",
          "info": ": object",
          "required": true,
          "schema": {
            "title": "body",
            "type": "object"
          }
        }
      ],
      "output": {
        "name": "result",
        "type": "object",
        "description": "A JSON Object containing status, code and the result",
        "schema": {
          "title": "result",
          "type": "object"
        }
      },
      "roles": [
        "admin"
      ],
      "route": {
        "verb": "POST",
        "path": "/postUpsertMetadata"
      },
      "task": true
    }
  ],
  "views": []
}